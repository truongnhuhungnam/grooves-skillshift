import luxy from "luxy.js";
import { isMobile } from "./isMobile";

/*
 *
 * ----------------------------------------------- */
jQuery(window).on("load", function () {
  if (!isMobile()) {
    if ($("#luxy").length) {
      luxy.init();
    }
  }
});
