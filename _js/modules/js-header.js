import { isMobile } from "./isMobile";

/*
 * Fixed position header
 * ----------------------------------------------- */
jQuery(window).on("load scroll", function () {
  if (isMobile()) {
    return false;
  }

  var $jsHeader = $(".js-header");

  $jsHeader.each(function () {
    var scroll = $(window).scrollTop();
    var formOffset = $("#form-title").offset().top;

    if (scroll > 500) {
      $jsHeader.addClass("is-scrolled");
    } else {
      $jsHeader.removeClass("is-scrolled");
    }

    if (scroll > 600) {
      $jsHeader.addClass("is-transition");
    } else {
      $jsHeader.removeClass("is-transition");
    }

    if (formOffset - 400 > scroll && scroll > 700) {
      $jsHeader.addClass("is-show");
    } else {
      $jsHeader.removeClass("is-show");
    }
  });

  function getMobileOperatingSystem() {
    var userAgent = navigator.userAgent || navigator.vendor || window.opera;
    // Windows Phone must come first because its UA also contains "Android"
    if (/windows phone/i.test(userAgent)) {
      return "Windows Phone";
    }
    if (/android/i.test(userAgent)) {
      return "Android";
    }
    // iOS detection from: http://stackoverflow.com/a/9039885/177710
    if (/iPad|iPhone|iPod|Mac/.test(userAgent) && !window.MSStream) {
      return "iOS";
    }
    return "unknown";
  }

  $("body").addClass(getMobileOperatingSystem());

  getMobileOperatingSystem();
});
