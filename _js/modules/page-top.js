import "jquery-smooth-scroll";
import "jquery.easing";

/*
 *
 * ----------------------------------------------- */
jQuery(function ($) {
  $(window).scroll(function (event) {
    if ($(this).scrollTop() > 80) {
      $(".page-top").fadeIn(400);
    } else {
      $(".page-top").fadeOut(400);
    }
    if (
      $(window).scrollTop() + $(window).height() >
      $(".section-contact").offset().top + 95
    ) {
      $(".page-top").addClass("anchor");
    } else {
      $(".page-top").removeClass("anchor");
    }
  });

  $(".page-top").on("click", function (event) {
    $.smoothScroll({
      easing: "swing",
      speed: 400,
    });

    return false;
  });
});
